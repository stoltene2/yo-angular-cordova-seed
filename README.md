
How to use
==========

Install Ruby 1.9+, the compass gem, and nodejs.

Clone this repo, then run

    npm install -g cordova
    npm install
    bower install
    grunt serve

Then go to town:

    cordova platform add android
    grunt phonegap:build phonegap:emulate && platforms/android/cordova/log


TODO
====
List requirements


THANKS
======
[There is a writeup](http://brian.olore.net/wp/2013/12/yo-angular-cordova-yay/) of how I created this project
Thanks to Daniel Simard’s awesome [grunt-angular-phonegap](https://github.com/dsimard/grunt-angular-phonegap)
