'use strict';

angular.module('fsCordova', [])
.service('CordovaService', ['$document', '$q',
  function($document, $q) {
    var d = $q.defer(),
        resolved = true;

    this.ready = d.promise;

    document.addEventListener('load', function() {
      resolved = true;
    });
  }]);
