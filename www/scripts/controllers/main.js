'use strict';

angular.module('helloworldApp')
  .controller('MainCtrl', function ($scope, CordovaService) {

    CordovaService.ready.then(function() {
      //console.log("CORDOVA IS READY -----------");
      jQuery('.text-muted').html('Woot Woot!'); //yes it's jQuery... shoot me!
    });

    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
